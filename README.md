** How the RTDS (Adafruit Audio FX Sound Board + 2X2W Amp WAV/OGG Trigger 16MB) works**

*NEED SOUND: 1 second at least to 3 seconds at most.*
*A minimum sound level of 80 dBA, fast weighting IN.4.6*
*Easily recognizable. No animal voices, song parts or sounds that could be interpreted as*
*offensive will be accepted*
        As of 7th July 2021

The sound board is designed to be simple: it does not have polyphonic ability, **can't play MP3's** (MP3 is patented and costs $ to license, so this board uses the similar but not-patented OGG format, there's tons of free converters that will turn an MP3 into OGG), isn't reprogrammable or scriptable, and you can't have any other kind of trigger type. However, there's a good chance the project you want to make will work great.

Five different trigger effects - by changing the name of the files, you can create five different types of triggers which will cover a large range of projects without any programming

What do we mean by trigger effects? Well, depending on your project you may need to have audio play in different ways. We thought of the five most common needs and built it into the Sound Board so you just rename the file to get the effect you want. See the product tutorial for more details

*Basic Trigger* - name the file **Tnn.WAV or Tnn.OGG** to have the audio file play when the matching trigger pin nn is connected to ground momentarily

*Hold Looping Trigger* - name the file **TnnHOLDL.WAV or .OGG** to have the audio play only when the trigger pin is held low, it will loop until the pin is released

*Latching Loop Trigger* - name the file **TnnLATCH.WAV or .OGG** to have the audio start playing when the button is pressed momentarily, and repeats until the button is pressed again

*Play Next Trigger* - have up to 10 files play one after the other by naming them **TnnNEXT0.WAV thru TnnNEXT9.OGG**. Will start with #0 and each one on every momentary button press until it gets through all of them, then go back to #0

*Play Random Trigger* - just like the Play Next trigger, but will play up to 10 files in random order (**TnnRAND0.OGG thru TnnRAND9.OGG**) every time the button is pressed momentarily

**power from 3-5VDC** so a 3xAAA battery pack or a LiPoly battery will work well. You can even use our LiPoly backpack to fit on top for an all-in-one rechargeable effects board

Note: The terminal blocks included with your product may be blue or black.

This product includes the Audio FX sound board, header strip, and two 2-pin terminal blocks.  It does not include batteries, speakers, breadboard, or tactile switches.

The Sound Board has a lot of amazing features that make it the easiest thing ever:

No Arduino or other microcontroller required! It is completely stand-alone, just needs a 3 to 5.5VDC battery

Small - only 1.9" x 0.85"

Built in storage - yep! you don't even need an SD card, there's 16MB of storage on the board itself. Good for a few minutes of compressed stereo, and maybe half a minute of uncompressed stereo. Double that if you go with mono instead of stereo 

Built in Mass Storage USB - Plug any micro USB cable into the Sound Board and your Windows computer, you can drag and drop your files right on as if it were a USB key
Compressed or Uncompressed audio - Go with compressed Ogg Vorbis files for longer audio files, or uncompressed WAV files

High Quality Sound - You want 44.1KHz 16 bit stereo? Not a problem! The decoding hardware can handle any bit/sample rate and mono or stereo

11 Triggers - Connect up to 11 buttons or switches, each one can trigger audio files to play

Stereo line out - We breakout the line out levels so you can connect up headphones if desired

2 x 2W Class D Amplifier - Get booming instantly, we baked in a stereo amplifier that can drive 4-8 ohm speakers, up to 2.2W with only 1% distortion
*Please note you cannot connect these to another amplifier. The built in amp means they are for direct-to-speaker connection only.*




**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).